async function getClient() {
  const response = await fetch("./client.xml");
  clientData = await response.text();
  const parser = new DOMParser();
  const xml = parser.parseFromString(clientData, "text/xml");
  xmlToObj(xml);
}

function xmlToObj(xml) {
  const clientsArray = [];
  const clients = [...xml.querySelectorAll("ObjectClient")];
  clients.forEach((ObjectClient) => {
    clientsArray.push({
      idClient: ObjectClient.querySelector("idClient").textContent,
      raisonSociale: ObjectClient.querySelector("raisonSociale ").textContent,
      ville: ObjectClient.querySelector("ville").textContent,
      codePostal: ObjectClient.querySelector("codePostal").textContent,
    });
  });
  console.log(clientsArray);
}
getClient();
